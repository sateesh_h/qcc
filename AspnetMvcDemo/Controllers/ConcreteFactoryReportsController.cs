﻿using AspnetMvcDemo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspnetMvcDemo.Controllers
{
    public class ConcreteFactoryReportsController : Controller
    {
        QCEntities db = new QCEntities();

        // GET: ConcreteFactoryReports
        public ActionResult ConcreteFactoryReports(int id)
        {
            var factory = db.Factory11.Where(f => f.Id == id).FirstOrDefault();
            var samples = db.ConcreteSample1.OrderByDescending(s => s.ReportNo).ToList();
            var lastSample = samples.Count() == 0 ? null : samples.FirstOrDefault();

            ConcreteSample1 sample = new ConcreteSample1();

            sample.SampleNumber = lastSample == null ? 5001 : lastSample.SampleNumber + 1;
            sample.ReportNo = lastSample == null ? 6001 : lastSample.ReportNo + 1;
            sample.VisitNumber = lastSample == null ? 3001 : lastSample.VisitNumber + 1;
            sample.FactoryName = factory.Name;
            sample.FactoryLocation = factory.Location;
            //sample.ClientName = factory.OwnerName;
            return View(sample);
        }

        [HttpPost]
        public ActionResult AddConcreteSample(ConcreteSample1 sample)
        {
            ObjectParameter statusCode = new ObjectParameter("StatusCode", typeof(int));
            ObjectParameter statusMessage = new ObjectParameter("StatusMessage", typeof(string));

            
            if (Request.Files.Count > 0)
            {
                foreach(var file in Request.Files)
                {
                    byte[] docData = null;
                    HttpPostedFileBase docFile = Request.Files["Doc"];

                    using (var binary = new BinaryReader(docFile.InputStream))
                    {
                        docData = binary.ReadBytes(docFile.ContentLength);
                    }
                }
                
            }
            var uId = Convert.ToInt32(Session["UserId"].ToString());
            db.AddUpdateConcreteSample(sample.ReportNo, sample.ReportDate, sample.FactoryName, sample.FactoryLocation, sample.MixerNumber, sample.VisitNumber,
                                       sample.SampleNumber, sample.TruckNumber, sample.InvoiceNumber, sample.ClientName, sample.VisitLocation, sample.Latitude,
                                       sample.Longitude, sample.ConcreteRank, sample.ConcreteTemperture, sample.WaterTemperature, sample.WeatherTemperture,
                                       sample.DownAmount, sample.CementType, sample.CementSource, sample.AdditionType, sample.AdditionAmount, sample.CementWeight,
                                       sample.WaterWieght, sample.WashedSandWeight, sample.WhiteSandWeight, sample.Rubble3by4, sample.Rubble3by8, sample.IsCleanUsage,
                                       sample.IsBasicUsage, sample.IsColumnUsage, sample.IsRoofUsage, sample.IsOtherUsage, sample.IsCleanLocation, sample.CleanDoc,
                                       sample.IsDustControlInStation, sample.DustDoc, sample.IsRokamSummer, sample.SummerDoc, sample.IsLabEngineer, sample.LabDoc,
                                       sample.IsMoldanatInTrucks, sample.TruckDoc, sample.IsPeopleSafty, sample.SafteyDoc, uId, null, statusCode, statusMessage);


            return View("ConcreteFactoryReports");
        }

        public ActionResult UpdateConcreteReport(int id)
        {
            ObjectParameter statusCode = new ObjectParameter("StatusCode", typeof(int));
            ObjectParameter statusMessage = new ObjectParameter("StatusMessage", typeof(string));

            var userId = Convert.ToInt32(Session["UserId"].ToString());

            var factoryName = db.Factory11.Where(f => f.Id == id).Select(x => x.Name).FirstOrDefault();

            var sampleNumber = from s in db.ConcreteSample1
                                 join v in db.VisitDetails on s.SampledBy equals v.MonitorId
                                 where v.MonitorId == userId && DbFunctions.DiffDays(v.VisitDate, DateTime.Now) == 1 && v.FactoryId==id && s.FactoryName== factoryName
                               select new List<long?> { s.SampleNumber};
            var lst = sampleNumber.ToList();

            var receivedSample = lst.Count()>0?db.UpdateConcreteReport(sampleNumber.FirstOrDefault().FirstOrDefault(), statusCode, statusMessage):0;

            return RedirectToAction("Home", "Home", new
            {
                id = 1
            });
        }


        public FileContentResult Docs()
        {
            // to get the user details to load user Image
            String Username = Session["Username"].ToString();

            var userImage = db.Users.Where(x => x.Username == Username).FirstOrDefault();
            if (userImage.Photo != null)
            {
                return new FileContentResult(userImage.Photo, "image/jpeg");
            }
            else
            {
                string fileName = HttpContext.Server.MapPath(@"~/Images/avatars/UserIcon.jpg");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);

                return File(imageData, "image/jpeg");
            }


        }

        public ActionResult ConcreteSampleReports()
        {
            var samples = db.ConcreteSample1.OrderByDescending(r => r.ReportDate).ToList();
            List<SampleReport> result = new List<SampleReport>();
            SampleReport s;
            if (samples.Count() > 0)
            {
                foreach (var sample in samples)
                {
                    s = new SampleReport
                    {
                        SampleNumber = sample.SampleNumber,
                        ReportedDate = sample.ReportDate,
                        FactoryName = sample.FactoryName,
                        MonitorName = db.Users.Where(u => u.Id == sample.SampledBy).FirstOrDefault().FullName,
                        CustomerName = sample.ClientName
                    };
                    result.Add(s);
                }
            }
            
            return View(result);
        }

        public ActionResult EditConcreteSampleReports(long Id)
        {
            ConcreteSample1 sample = db.ConcreteSample1.Where(s=>s.SampleNumber==Id).FirstOrDefault();
            
            return View(sample);
        }

        [HttpPost]
        public ActionResult UpdateConcreteSample(ConcreteSample1 sample)
        {
            ObjectParameter statusCode = new ObjectParameter("StatusCode", typeof(int));
            ObjectParameter statusMessage = new ObjectParameter("StatusMessage", typeof(string));


            if (Request.Files.Count > 0)
            {
                foreach (var file in Request.Files)
                {
                    byte[] docData = null;
                    HttpPostedFileBase docFile = Request.Files["Doc"];

                    using (var binary = new BinaryReader(docFile.InputStream))
                    {
                        docData = binary.ReadBytes(docFile.ContentLength);
                    }
                }

            }

            db.UpdateConcreteSample(sample.ReportNo, sample.ReportDate, sample.FactoryName, sample.FactoryLocation, sample.MixerNumber, sample.VisitNumber,
                                       sample.SampleNumber, sample.TruckNumber, sample.InvoiceNumber, sample.ClientName, sample.VisitLocation, sample.Latitude,
                                       sample.Longitude, sample.ConcreteRank, sample.ConcreteTemperture, sample.WaterTemperature, sample.WeatherTemperture,
                                       sample.DownAmount, sample.CementType, sample.CementSource, sample.AdditionType, sample.AdditionAmount, sample.CementWeight,
                                       sample.WaterWieght, sample.WashedSandWeight, sample.WhiteSandWeight, sample.Rubble3by4, sample.Rubble3by8, sample.IsCleanUsage,
                                       sample.IsBasicUsage, sample.IsColumnUsage, sample.IsRoofUsage, sample.IsOtherUsage, sample.IsCleanLocation, sample.CleanDoc,
                                       sample.IsDustControlInStation, sample.DustDoc, sample.IsRokamSummer, sample.SummerDoc, sample.IsLabEngineer, sample.LabDoc,
                                       sample.IsMoldanatInTrucks, sample.TruckDoc, sample.IsPeopleSafty, sample.SafteyDoc, 2, null, statusCode, statusMessage);


            return RedirectToAction("ConcreteSampleReports", "ConcreteFactoryReports");
        }
    }
}