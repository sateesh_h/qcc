﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspnetMvcDemo.Models;
using Resources;

namespace AspnetMvcDemo.Controllers
{    
    public class HomeController : Controller
    {
        QCEntities db = new QCEntities();

        // GET: Home
        //public ActionResult Home()
        //{
        //    return View();
        //}

        public ActionResult MakeChoice()
        {
            return View();
        }

        public ActionResult Home(int id)
        {
            Session["Choice"] = id == 1 ? "Concrete" : "Block";

            var userId = Convert.ToInt32(Session["UserId"].ToString());

            VisitDetailsModel model = new VisitDetailsModel();

            var visits = from f in db.Factory11
                         join v in db.VisitDetails on f.Id equals v.FactoryId
                         where v.MonitorId==userId && DbFunctions.TruncateTime(v.VisitDate)== DbFunctions.TruncateTime(DateTime.Now)
                         select f;
            model.TodayVisits = visits.ToList();

            var totalVisits = from f in db.Factory11
                         join v in db.VisitDetails on f.Id equals v.FactoryId
                         join u in db.Users on v.MonitorId equals u.Id
                         where DbFunctions.TruncateTime(v.VisitDate) >= DbFunctions.TruncateTime(DateTime.Now)
                         orderby v.VisitDate
                         select new AdminVisit {Id=v.Id, Monitor= u.FullName, VisitDate= DbFunctions.TruncateTime(v.VisitDate), FactoryName=f.Name, FactoryLocation=f.Location};

            model.TotalVisits = totalVisits.ToList();

            var receiveSamples = from f in db.Factory11
                         join v in db.VisitDetails on f.Id equals v.FactoryId
                         join c in db.ConcreteSample1 on f.Name equals c.FactoryName
                         where v.MonitorId == userId && DbFunctions.DiffDays(v.VisitDate, c.ReportDate) == 0 && DbFunctions.DiffDays(v.VisitDate, DateTime.Now) == 1
                         select f;

            model.ReceiveSamples = receiveSamples.ToList();

            var brokenSamples = from f in db.Factory11
                                 join v in db.VisitDetails on f.Id equals v.FactoryId
                                 where v.MonitorId == userId && DbFunctions.DiffDays(v.VisitDate, DateTime.Now) == 28
                                 select f;

            model.BrokenSamples = brokenSamples.ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult VisitDetails(int Id)
        {
            var visitDetails = from f in db.Factory11
                              join v in db.VisitDetails on f.Id equals v.FactoryId
                              join u in db.Users on v.MonitorId equals u.Id
                              where v.Id== Id
                              orderby v.VisitDate
                              select new AdminVisit { MonitorId=u.Id, FactoryId=f.Id, Monitor =u.FullName, VisitDate = DbFunctions.TruncateTime(v.VisitDate), FactoryName=f.Name, FactoryLocation=f.Location };

            var monitors = db.Users.Where(u => u.JobTitle == "Monitor").Select(x => new Mon { MonitorId = x.Id, MonitorName = x.FullName }).ToList().ToList();

            var visit = visitDetails.FirstOrDefault();

            AdminVisit result = new AdminVisit
            {
                Id=Id,
                MonitorId = visit.MonitorId,
                FactoryId=visit.FactoryId,
                Monitor = visit.Monitor,
                FactoryName=visit.FactoryName,
                FactoryLocation=visit.FactoryLocation,
                VisitDate=visit.VisitDate,
                RemainingMonitors=monitors
            };

            return PartialView(result);
        }

        [HttpPost]
        public ActionResult UpdateVisit(VisitDetail visit)
        {
            ObjectParameter statusCode = new ObjectParameter("StatusCode", typeof(int));
            ObjectParameter statusMessage = new ObjectParameter("StatusMessage", typeof(string));

            var result = db.AddUpdateVisit(visit.Id, visit.FactoryId, visit.MonitorId, visit.VisitDate, statusCode, statusMessage);

            return RedirectToAction("Home", "Home", new
            {
                id = 1
            });
        }
    }
}