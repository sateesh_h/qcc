﻿using AspnetMvcDemo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspnetMvcDemo.Controllers
{
    public class LaboratoryController : Controller
    {
        QCEntities db = new QCEntities();

        // GET: Laboratory
        public ActionResult Laboratory(int id)
        {
            Session["Choice"] = id == 1 ? "Concrete" : "Block";

            LaboratoryModel model = new LaboratoryModel();

            model.TodaySamples = db.ConcreteSample1.Where(s => s.IsReceived == true && DbFunctions.DiffDays(s.CreatedDate, DateTime.Now) == 1).ToList();

            model.SevenDaysSamples = db.ConcreteSample1.Where(s => s.IsReceived == true && DbFunctions.DiffDays(s.CreatedDate, DateTime.Now) == 7).ToList();

            model.TestedSamples = db.ConcreteSample1.Where(s => s.IsReceived == true && DbFunctions.DiffDays(s.CreatedDate, DateTime.Now) == 28).ToList();

            return View(model);
        }

      
    }
}