﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AspnetMvcDemo.Models
{
    public class CreateFactory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public Nullable<double> ProductionCapacity { get; set; }
        public Nullable<double> DailyProductionRate { get; set; }
        public Nullable<int> NumberofMixers { get; set; }
        public Nullable<int> NumberofTrucks { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string MailBox { get; set; }
        public string Email { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<double> Longitude { get; set; }
        public string OwnerName { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerPhoneNumber { get; set; }
        public string ManagerName { get; set; }
        public string ManagerPhoneNumber { get; set; }
        public string ManagerEmail { get; set; }
        public Nullable<double> ManagerExperience { get; set; }
        public string EngineerName { get; set; }
        public string EngineerPhoneNumber { get; set; }
        public string EngineerEmail { get; set; }
        public Nullable<double> EngineerExperience { get; set; }
    }

    public class MixingDesignDetails
    {
        public string Name { set; get; }
        public string Area { set; get; }
        public string Owner { set; get; }
        public string ConcreteRank { set; get; }
        public string Comments { set; get; }
        public string Status { set; get; }
    }

    public class Fact
    {
        public int FactoryId { get; set; }
        public string FactoryName { get; set; }
    }

    public class Mon
    {
        public int MonitorId { get; set; }
        public string MonitorName { get; set; }
    }

    public class AddMixDesign : ConcreteMixingDesign
    {
        public List<Fact> Factories { get; set; }
    }

    public class ApproveMixingDesign : ConcreteMixingDesign
    {
        public string FactoryName { get; set; }
        public string OwnerName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }

    public class AdminVisit
    {
        [Key]
        public long Id { get; set; }
        public long MonitorId { get; set; }
        public long FactoryId { get; set; }
        public string Monitor { get; set; }
        public DateTime? VisitDate { get; set; }
        public string FactoryName { get; set; }
        public string FactoryLocation { get; set; }
        public List<Mon> RemainingMonitors { get; set; }
    }

    public class EditVisit
    {
        public List<string> Monitor { get; set; }
        public List<string> FactoryName { get; set; }
        public DateTime? VisitDate { get; set; }
    }

    public class VisitDetailsModel
    {
        public List<Factory11> TodayVisits { get; set; }
        public List<Factory11> ReceiveSamples { get; set; }
        public List<Factory11> BrokenSamples { get; set; }

        public List<AdminVisit> TotalVisits { get; set; }
    }

    public class LaboratoryModel
    {
        public List<ConcreteSample1> TodaySamples { get; set; }
        public List<ConcreteSample1> SevenDaysSamples { get; set; }
        public List<ConcreteSample1> TestedSamples { get; set; }
    }

    public class SampleReport
    {
        [Key]
        public long? SampleNumber { get; set; }
        public DateTime? ReportedDate { get; set; }
        public string FactoryName { get; set; }
        public string MonitorName { get; set; }
        public string CustomerName { get; set; }
    }
}